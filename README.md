# Fermentation Graphs

## Build Setup

This assumes you have Node >=14 installed.

- >=14 because I'm using the optional changing operator
- >=12 as required by nuxt

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how Nuxt does things, check out the [Nuxt documentation](https://nuxtjs.org).

## Data

Fermentor data for demonstration purposes is stored in `/data/` along with a file describing the measurement types.

With a running server, the data is accessible from:

- http://localhost:3000/data/fermentors
- http://localhost:3000/data/fermentor/[fermentor_id]

This is exposed by `server-middleware/get-data.js`.

## Frontend

## Task completion

- > Users should be able to see all fermentors.
    - Implemented. With a running server, the status of the fermentors is visible at http://localhost:3000/ . Depending on how many fermentors there are, navigation between them might be more appropriate, rather than having them load in series.
- > Users should be able to see all fermentation runs for each fermentor.
    - Implemented. Users can see one run at a time per fermentor; a `select` is available; the most recent run is shown by default.
- For each fermentation run, users should be able to easily know whether it was in the past or if it is currently running.
    - Implemented.
- > For past and active fermentation runs, users should be able to see a graph showing all values of all measuring events (temperature, dissolved oxygen and pH) received for that fermentation run. This is the most important feature of all.
    - Implemented.
    - > Being able to zoom, isolate labels, etc, would be a bonus.
      - **Not implemented**. If the data is representative, I'd prioritise making it easier to get precise values from the graph.
    - > For active fermentation runs, users would also like to see the last value received for each of the sensors.
      - Implemented (regardless of whether the run is completed). Would be nice to align these with the graph.
- > Consider using Docker and Docker Compose for deployment!
    - I considered it! I didn't have time within the allotted time and it's not something I'm using day-to-day, but I've been looking for opportunities to try it out so I had a go afterwards - looks like the config for a single service that doesn't have to talk to anything else is be pretty straightforward. `docker compose build && docker compose up -d` appears to produce a development server running on :3000.

There remain many opportunities for optimisations, affordances, and refactoring, a few of which are referenced in code comments.

I divided my time into three chunks of approximately an hour and a half each, not including a bit of time earlier in the week to take a copy of the description and data, and the time writing this up at the end (and briefly experimenting with Docker).