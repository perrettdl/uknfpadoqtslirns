const app           = require('connect')();
const fermentors    = require('../data/fermentors.json');
const fermentorById = fermentors.reduce(
  (acc, id) => Object.assign(acc, {
    // dynamic require as a storage layer is purely for demonstration purposes!
    [id]: require(`../data/${id}.json`)
  }),
  {}
);

// Simulate a client-server architecture using nuxt middleware.
// connect provides rudimentary route handling, so we won't be doing
// anything particularly elegant or scalable.
app.use('/fermentors', (req, res) => res.end(JSON.stringify(fermentors)));

app.use('/fermentor/', (req, res) => {
  const id = req.url.split('/')[1];

  // Minimal validation here, as we know the set of valid values in advance
  if (Object.prototype.hasOwnProperty.call(fermentorById, id)) {
    res.end(JSON.stringify(fermentorById[id]));
  }

  res.statusCode = 404;
  res.end();
});

module.exports = app;