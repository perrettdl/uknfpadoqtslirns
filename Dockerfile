FROM node:14-alpine

WORKDIR /app
COPY package.json ./app
COPY . /app

# We have a yarn.lock, so first get yarn
RUN apk add yarn
RUN yarn install

EXPOSE 3000

ENV NUXT_HOST=0.0.0.0
ENV NUXT_PORT=3000
