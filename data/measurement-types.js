
module.exports = [
  {
    eventType       : 'MeasureDO',
    eventProperty   : 'DissolvedOxygen',
    eventShortLabel : 'DO',
    eventUnit       : '%', // not sure if this is the unit, but let's use it
    maxValue        : 30,
    minValue        : 0,
    lineValues      : [ 10, 20 ],
  },
  {
    eventType       : 'MeasurePh',
    eventProperty   : 'pH',
    eventShortLabel : 'pH',
    eventUnit       : '',
    maxValue        : 14,
    minValue        : 0,
    lineValues      : [ 7 ],
  },
  {
    eventType       :'MeasureTemperature',
    eventProperty   :'Temperature',
    eventShortLabel :'Temp',
    eventUnit       :'°C',
    maxValue        : 40,
    minValue        : 20,
    lineValues      : [ 25, 30, 35 ],
  }
];
